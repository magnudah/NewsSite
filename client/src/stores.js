// @flow
import { sharedComponentData } from 'react-simplified';
import axios from 'axios';
axios.interceptors.response.use(response => response.data);

export class Sak {
    id: number = 0;
    overskrift: string = '';
    innhold: string = '';
    tidOpprettelse: Date;
    bilde: string = '';
    kat_navn: string = '';
    viktighet: string = '';
}



class Kat {
    kat_navn: string = '';

}

class SakStore {
  saker = [];
  feedSaker = [];
  kategorier = [];
  currentKat = new Kat();
  currentSak = new Sak();
  oppdaterCurrentSak = new Sak();


  getSaker() {
    return axios.get('/nyheter').then((saker: Sak[]) => (this.saker = saker));
  }

  getArticles(){
      return axios.get('/all').then((saker: Sak[]) => (this.saker = saker));
  }

  getKategorier() {
    return axios.get('/kategori').then((kategorier: Kat[]) => (this.kategorier = kategorier));
  }

  getSakerInKat(kategori: string) {
    return axios.get('/kategori/' + kategori).then((saker: Sak[]) => (this.saker = saker));
  }

  getSak(id: number) {
    return axios.get('/nyheter/' + id).then((sak: Sak) => {
      //console.log(sak);
      /*for (let e of this.saker) {
        if (e.id == sak.id) {
          e.overskrift = sak.overskrift;
          e.innhold = sak.innhold;
          e.bilde = sak.bilde;
          e.tidOpprettelse = sak.tidOpprettelse;
          break;
        }
      }*/
      this.currentSak = sak;
    });
  }

  getNewsfeed(){
      return axios.get('/newsfeed').then((feedSaker: Sak[]) => (this.feedSaker = feedSaker));
  }

  updateSak(id: number) {
    return axios.put('/edit/' + id, this.oppdaterCurrentSak).then(() => {
      for (let e of this.saker) {
        if (e.id == this.oppdaterCurrentSak.id) {
          e.overskrift = this.oppdaterCurrentSak.overskrift;
          e.innhold = this.oppdaterCurrentSak.innhold;
          e.bilde = this.oppdaterCurrentSak.bilde;
          break;
        }
      }
    });
  }

  createArticle(){
      return axios.post('/nyheter', this.currentSak).then((sak: Sak) => {
          sak.overskrift= this.currentSak.overskrift;
          sak.innhold = this.currentSak.innhold;
          sak.bilde = this.currentSak.bilde;
          sak.viktighet = this.currentSak.viktighet;
          sak.kat_navn = this.currentSak.kat_navn;
      });
  }

  deleteArticle(id:number){
    return axios.delete('/edit/' + id, id);
  }
}
export let sakStore = sharedComponentData(new SakStore());
