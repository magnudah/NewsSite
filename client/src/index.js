// @flow
/* eslint eqeqeq: "off" */

import * as React from 'react';
import { Component } from 'react-simplified';
import { HashRouter, Route, NavLink } from 'react-router-dom';
import ReactDOM from 'react-dom';
import { Alert } from './widgets';
import {sakStore} from './stores';
import "/Users/Dahly/Desktop/HTML/src/oving3/client/public/styling.css";

if (process.env.NODE_ENV !== 'production') {
    let script = document.createElement('script');
    script.src = '/reload/reload.js';
    if (document.body) document.body.appendChild(script);
}

import createHashHistory from 'history/createHashHistory';
const history = createHashHistory();


class Menu extends Component {
    render() {
        return (
            <div className="card text-center">
                <div className="card-header">
                        <a className="nav-link" href="#">
                            <p className="text-black-50">
                            <h1><b>STUDENTAVISA</b></h1>
                            </p>
                        </a>
            <nav className="navbar navbar-expand-lg navbar-light">
                <a className="navbar-brand" href="#"/>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"/>
                </button>



                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav navbar-left">
                        <li className="nav-item active">
                            <a className="nav-link" href="#"><h4>Forside</h4><span className="sr-only">(current)</span></a>
                        </li>
                        {sakStore.kategorier.map(kat => (
                                <li className="nav-item">
                                    <a className="nav-link" href={'#kategorier/' + kat.kat_navn}><h4>{kat.kat_navn}</h4></a>
                                </li>
                        ))}
                    </ul>
                    <ul className="navbar-nav navbar-right">
                        <li className="nav-item">
                            <p className="navbar-btn">
                                <a className="nav-link btn btn-outline-primary" href="#add"><h4>Legg til sak</h4></a>
                            </p>
                        </li>
                    </ul>
                </div>

            </nav>
                </div>
            </div>
        );
    }

    mounted() {
        sakStore.getKategorier().catch((error: Error) => Alert.danger(error.message));
    }
}

class NewsFeed extends Component{

    render(){
        return(
                <div className="row">
                    <marquee behavior="scroll" direction="left">
                        {sakStore.feedSaker.map(sak => (
                                <span className="panel-heading">
                                    <span  key={sak.id}>
                                      <NavLink style={{color: 'black'}} activeStyle={{ color: 'dark' }} exact to={'/nyheter/' + sak.id}>
                                        <b>{sak.overskrift}</b> {sak.tidOpprettelse}
                                      </NavLink>{' '}
                                    </span>
                                </span>
                        ))}
                    </marquee>
                </div>
        );
    }
    mounted() {
        sakStore.getNewsfeed().catch((error: Error) => Alert.danger(error.message));
    }
}


function Card(props){
    return(
      <div className="card border-dark mp-3">
        <img className="card-img-top" src={props.image} alt={props.alt}/>
          <div className="card-body">
            <h5 className="card-title">{props.title}</h5>
          </div>
      </div>
    )
}

class MainFeed extends Component{
    render(){
        return(
              <div className="container">
                <div className = "row">
                    {sakStore.saker.map(sak => (
                      <div className="col-4">
                            <div key={sak.id}>
                                <NavLink style={{color: 'black'}} activeStyle={{ color: 'dark' }} exact to={'/nyheter/' + sak.id}>
                                    <Card title = {sak.overskrift} image= {sak.bilde} alt={sak.overskrift}/>
                                </NavLink>{' '}
                            </div>
                      </div>
                    ))}
                </div>
              </div>

        );
    }
    mounted() {
        sakStore.getSaker().catch((error: Error) => Alert.danger(error.message));
    }
}

class CategoryFeed extends Component<{ match: { params: { id: string } } }>{
  render(){
    return(
      <div className = "container">
        <div className = "row">
          {sakStore.saker.map(sak => (
            <div className="col-4">
              <div key={sak.kat_navn}>
                <NavLink style={{color: 'black'}} activeStyle={{ color: 'dark' }} exact to={'/nyheter/' + sak.id}>
                  <Card title = {sak.overskrift} image= {sak.bilde} alt={sak.overskrift}/>
                </NavLink>{' '}
              </div>
            </div>
          ))}
        </div>
      </div>
    );
  }
  mounted() {
    sakStore.getSakerInKat(this.props.match.params.id).catch((error: Error) => Alert.danger(error.message));
  }
}

class Article extends Component <{ match: { params: { id: number } } }>{
    render() {
        return (
            <form>
                <div className="container text-center">
                    <img className="img-fluid" src = {sakStore.saker.filter(e => e.id == this.props.match.params.id)[0].bilde}/>

                    <p>{sakStore.saker.filter(e => e.id == this.props.match.params.id)[0].tidOpprettelse}</p>

                    <h1>{sakStore.saker.filter(e => e.id == this.props.match.params.id)[0].overskrift}</h1>


                    <hr/>
                    <p>{sakStore.saker.filter(e => e.id == this.props.match.params.id)[0].innhold}</p>
                    <hr/>
                </div>
            </form>
        );
    }
    mounted() {
        sakStore.getSak(this.props.match.params.id).catch((error: Error) => Alert.danger(error.message));
    }
}


class LeggTilSak extends Component{
    render() {
        return (
            <form>
                <div className="container">
                    <span>
                    <h1>Legg til sak</h1>
                        <p align="right">
                            <a className="btn btn-primary" href="#edit" role="button">Endre en sak</a>
                        </p>
                    </span>
                    <hr/>

                    <div className="form-group">
                        <label htmlFor="overskrift">Overskrift</label>
                        <input value={sakStore.currentSak.overskrift} type="overskrift" className="form-control" id="overskrift" placeholder="Din overskrift"
                               onChange={(event: SyntheticInputEvent<HTMLInputElement>) => (sakStore.currentSak.overskrift = event.target.value)}
                        />
                    </div>

                    <div className="form-group">
                      <label htmlFor="viktighet">Kategorier</label>
                      <select value={sakStore.currentSak.kat_navn} className="form-control" id="kat_navn"
                              onChange={(event: SyntheticInputEvent<HTMLInputElement>) => (sakStore.currentSak.kat_navn = event.target.value)}>
                        <option selected>Velg en kategori</option>
                        {sakStore.kategorier.map(kat => (
                          <option>{kat.kat_navn}</option>
                        ))}
                      </select>
                    </div>

                    <div className="form-group">
                        <label htmlFor="innhold">Ditt innhold</label>
                        <textarea value={sakStore.currentSak.innhold} className="form-control" id="innhold" rows="4"
                                  onChange={(event: SyntheticInputEvent<HTMLInputElement>) => (sakStore.currentSak.innhold = event.target.value)}
                        />
                    </div>

                  <div className="form-group">
                    <label htmlFor="viktighet">Viktighet</label>
                    <select value={sakStore.currentSak.viktighet} className="form-control" id="viktighet"
                            onChange={(event: SyntheticInputEvent<HTMLInputElement>) => (sakStore.currentSak.viktighet = event.target.value)}>
                      <option value='1' selected>1</option>
                      <option value='2'>2</option>
                    </select>
                  </div>

                    <div className="form-group">
                        <label htmlFor="bilde">Bildelink</label>
                        <input value={sakStore.currentSak.bilde} type="bilde" className="form-control" id="bilde" placeholder="bildelink"
                               onChange={(event: SyntheticInputEvent<HTMLInputElement>) => (sakStore.currentSak.bilde = event.target.value)}
                        />
                    </div>

                    <hr/>
                    <button type="button" className="btn btn-primary" onClick={this.add}>
                        Legg til
                    </button>
                </div>
            </form>
        );
    }

    add() {
    sakStore
        .createArticle()
        .then(() => history.push('/'))
        .catch((error: Error) => Alert.danger(error.message));
    }

    mounted(){
      sakStore.getKategorier().catch((error: Error) => Alert.danger(error.message));
    }
}


class SakListe extends Component <{ match: { params: { id: number } } }>{
    render() {
        return (
            <div className="container">
              <h2>Velg en sak for å endre</h2>
                <hr/>
                <div className = "list-group">
                    {sakStore.saker.map(sak => (
                      <a href={"#edit/" + sak.id} className="list-group-item list-group-item-action">{sak.tidOpprettelse} {' '} <b>{sak.overskrift}</b>
                        </a>
                    ))}
                </div>
                <hr/>
            </div>
        );
    }
    mounted() {
        sakStore.getArticles().catch((error: Error) => Alert.danger(error.message));
    }
}


class EndreSak extends Component <{ match: { params: { id: number } } }> {
    render() {
        return (
          <form>
            <div className="container">
              <h2>Endre din artikkel</h2>
              <hr/>
                <div className="form-group">
                  <label>Overskrift</label>
                  <input className="form-control"
                    type="text"
                    value={sakStore.oppdaterCurrentSak.overskrift}
                    onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                      (sakStore.oppdaterCurrentSak.overskrift = event.target.value)
                    }
                  />
                </div>

                <div className="form-group">
                  <label>Innhold</label>
                    <textarea className="form-control"
                      value={sakStore.oppdaterCurrentSak.innhold}
                      onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                        (sakStore.oppdaterCurrentSak.innhold = event.target.value)
                      }
                    />
                </div>


                <div className="form-group">
                  <label>Bilde</label>
                  <input className="form-control"
                    type="text"
                    value={sakStore.oppdaterCurrentSak.bilde}
                    onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                      (sakStore.oppdaterCurrentSak.bilde = event.target.value)
                    }
                  />
                </div>

                    <button type="button" className="btn btn-primary" onClick={this.save}>
                        Lagre
                    </button>
              <div className="pull-right">
                <button type="button" className="btn btn-primary" onClick={this.delete}>
                  Slett artikkel
                </button>
              </div>
                    <hr/>
            </div>
          </form>
            );
        }

  mounted() {
    sakStore.getSak(this.props.match.params.id).catch((error: Error) => Alert.danger(error.message));
  }

    save() {
        sakStore
            .updateSak(this.props.match.params.id)
            .then(() => history.push('/edit'))
            .catch((error: Error) => Alert.danger(error.message));
    }

  delete(){
      console.log("hei");
    sakStore
      .deleteArticle(this.props.match.params.id)
      .then(() => history.push('/edit'))
      .catch((error: Error) => Alert.danger(error.message));
  }
}


const root = document.getElementById('root');
if (root)
    ReactDOM.render(
        <HashRouter>
            <div>
                <Menu />
                <NewsFeed />
                <Route exact path="/" component={MainFeed} />
                <Route exact path="/kategorier/:id" component={CategoryFeed} />
                <Route path="/nyheter/:id" component={Article} />
                <Route path="/add" component={LeggTilSak} />
                <Route exact path="/edit" component={SakListe} />
                <Route path="/edit/:id" component={EndreSak} />
            </div>
        </HashRouter>,
        root
    );
