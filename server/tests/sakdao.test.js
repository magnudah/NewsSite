// @flow

var mysql = require("mysql");

const SakDao = require("../src/dao/newsDao");
const runsqlfile = require("./runsqlfile.js");

var pool = mysql.createPool({
  connectionLimit: 1,
  host: "mysql",
  user: "root",
  password: "secret",
  database: "supertestdb",
  debug: false,
  multipleStatements: true
});

let sakDao = new SakDao(pool);

beforeAll(done => {
    runsqlfile(__dirname + "/create_tables.sql", pool, () => {
        runsqlfile(__dirname + "/create_testdata.sql", pool, done);
    });
});

test("Få en sak fra databasen", done => {
    function callback(status, data) {
        console.log(
            "Test callback: status=" + status + ", data=" + JSON.stringify(data)
        );
        expect(data.length).toBe(1);
        expect(data[0].overskrift).toBe("Test1");
        done();
    }

    sakDao.getOneArticle(1, callback);
});

test("Legg til en sak i databasen", done => {
    function callback(status, data) {
        console.log(
            "Test callback: status=" + status + ", data=" + JSON.stringify(data)
        );
        expect(data.affectedRows).toBeGreaterThanOrEqual(1);
        done();
    }

    sakDao.createArticle(
        { overskrift: "Legg til test", innhold: "legg til testinnhold", tidOpprettelse: "2018-09-04 15:18:10", bilde: "leggtil.png", kat_navn: "sport", viktighet: 1},
        callback
    );
});

test("få alle saker fra databasen", done => {
    function callback(status, data) {
        console.log(
            "Test callback: status=" + status + ", data.length=" + data.length
        );
        expect(data.length).toBeGreaterThanOrEqual(3);
        done();
    }

    sakDao.getArticles(callback);
});

test("Oppdater en sak", done => {
    function callback(status, data) {
        console.log(
            "Test callback: status=" + status + ", data=" + JSON.stringify(data)
        );
        //expect(data[0].overskrift).toBe("Test2");
        expect(data.affectedRows).toBeGreaterThanOrEqual(1);
        done();
    }

    sakDao.updateArticle(
        1,
        {overskrift: "Test2"},
        callback
    );
});

test("Slett en sak", done => {
    function callback(status, data) {
        console.log(
            "Test callback: status=" + status + ", data=" + JSON.stringify(data)
        );
        expect(data.affectedRows).toBe(1);
        done();
    }

    sakDao.deleteArticle(
        1,
        callback
    );
});
