CREATE TABLE sak (
  id integer NOT NULL auto_increment,
  overskrift varchar(255) NOT NULL,
  innhold varchar(20000),
  tidOpprettelse DATETIME NOT NULL,
  bilde varchar(2000),
  kat_navn varchar(200),
  viktighet integer NOT NULL,
  CONSTRAINT pk_sak PRIMARY KEY(id)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;