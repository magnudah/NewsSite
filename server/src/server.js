// @flow

import express from 'express';
import path from 'path';
import reload from 'reload';
import fs from 'fs';

var mysql = require("mysql");
var app = express();
var bodyParser = require("body-parser");
app.use(bodyParser.json());
const NewsDao = require("./dao/newsDao.js");

type Request = express$Request;
type Response = express$Response;

const public_path = path.join(__dirname, '/../../client/public');

app.use(express.static(public_path));
app.use(express.json());

//var express = require("express");

var pool = mysql.createPool({
    connectionLimit: 2,
    host: "mysql.stud.iie.ntnu.no",
    user: "magnudah",
    password: "pGXAIkjI",
    database: "magnudah",
    debug: false,
    multipleStatements: true
});

let newsDao = new NewsDao(pool);


app.get("/nyheter", (req: Request, res: Response) => {
    console.log("/nyheter: fikk request fra klient");
    newsDao.getImportentNews((status, data) => {
        res.status(status);
        res.json(data);
    });
});

app.get("/nyheter/:id", (req: Request, res: Response) => {
    console.log("/nyheter/:id: fikk request fra klient");
    newsDao.getOneArticle(req.params.id, (status, data) => {
        res.status(status);
        res.json(data);
    });
});


app.post("/nyheter", (req: Request, res: Response) => {
    console.log("Fikk POST-request fra klienten");
    newsDao.createArticle(req.body, (status, data) => {
        res.status(status);
        res.json(data);
    });
});

app.put("/edit/:id", (req: Request, res: Response) => {
  console.log("/edit/:id: Fikk PUT-request fra klienten");
  newsDao.updateArticle(req.params.id,req.body, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

app.delete("/edit/:id", (req: Request, res: Response) => {
    console.log("/nyheter/:id: Fikk DELETE-request fra klienten");
    newsDao.deleteArticle(req.params.id, (status, data) => {
        res.status(status);
        res.json(data);
    });
});

app.get("/kategori/:kat_navn", (req: Request, res: Response) => {
    console.log("/nyheter/kategori: Fikk GET-request fra klienten");
    newsDao.getArticlesInKat(req.params.kat_navn, (status, data) => {
        res.status(status);
        res.json(data);
    });
});

app.get("/kategori", (req: Request, res: Response) => {
    console.log("/nyheter/kategori: Fikk GET-request fra klienten");
    newsDao.getKat((status, data) => {
        res.status(status);
        res.json(data);
    });
});

app.get("/newsfeed", (req: Request, res: Response) => {
    console.log("/nyheter/newsfeed: Fikk GET-request fra klienten");
    newsDao.getNewsfeed((status, data) => {
        res.status(status);
        res.json(data);
    });
});

app.get("/all", (req: Request, res: Response) => {
    console.log("/nyheter/allesaker: Fikk GET-request fra klienten");
    newsDao.getArticles((status, data) => {
        res.status(status);
        res.json(data);
    });
});


// Hot reload application when not in production environment
if (process.env.NODE_ENV !== 'production') {
    let reloadServer = reload(app);
    fs.watch(public_path, () => reloadServer.reload());
}

// The listen promise can be used to wait for the web server to start (for instance in your tests)
/*export let listen = new Promise<void>((resolve, reject) => {
    app.listen(3000, error => {
        if (error) reject(error.message);
        console.log('Server started');
        resolve();
    });
});*/

app.listen(3000);