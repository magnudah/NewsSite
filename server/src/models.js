// @flow

/*import Sequelize from 'sequelize';
import type { Model } from 'sequelize';

let sequelize = new Sequelize('School', 'root', '', {
  host: process.env.CI ? 'mysql' : 'localhost', // The host is 'mysql' when running in gitlab CI
  dialect: 'mysql',

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
});


export let Saker: Class<
  Model<{ id?: number, overskrift: string, innhold: string, tidOpprettelse: string, bilde: string, kategori: string, viktighet: string }>
> = sequelize.define('saker', {
  id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  overskrift: Sequelize.STRING,
  innhold: Sequelize.STRING,
  tidOpprettelse: Sequelize.DATE,
    bilde: Sequelize.STRING,
    kategori: Sequelize.STRING,
    viktighet: Sequelize.INTEGER
});

// Drop tables and create test data when not in production environment
let production = process.env.NODE_ENV === 'production';
// The sync promise can be used to wait for the database to be ready (for instance in your tests)
export let sync = sequelize.sync({ force: production ? false : true }).then(() => {
  if (!production)
    return Saker.create({
        overskrift: 'Test overskrift',
        innhold: 'Dette er mitt test innhold',
        tidOpprettelse: '14.11.2018 22:50:00',
        bilde: 'test.png',
        kategori: 'sport',
        viktighet: '1'
    }).then(() =>
      Saker.create({
          overskrift: 'Test overskrift 2',
          innhold: 'Dette er mitt test innhold 2',
          tidOpprettelse: '15.11.2018 22:50:00',
          bilde: 'test2.png',
          kategori: 'innenriks',
          viktighet: '2'
      })
    );
});
*/