//@flow
const Dao = require("./dao.js");

module.exports = class newsDao extends Dao {

    getImportentNews(callback:any) {
        super.query("SELECT id, overskrift, innhold, DATE_FORMAT(tidOpprettelse, '%d.%b %Y %k:%i') as tidOpprettelse, bilde from sak WHERE viktighet = 1 ORDER BY id DESC LIMIT 20",
            [],
            callback);
    }

    getOneArticle(id:number, callback:any) {
        super.query(
            "SELECT id, overskrift, innhold, DATE_FORMAT(tidOpprettelse, '%d.%b %Y %k:%i') as tidOpprettelse, bilde  FROM sak WHERE id = ?",
            [id],
            callback
        );
    }

    updateArticle(id:number, json:any, callback:any){
        var val = [json.overskrift,json.innhold, json.bilde, id];
        super.query(
            "UPDATE sak SET overskrift = ?, innhold = ?, bilde = ? where id = ?",
            val,
            callback
        );
    }

    deleteArticle(id:number, callback:any){
        super.query(
            "DELETE FROM sak WHERE id = ?",
            [id],
            callback
        );
    }

    createArticle(json:any, callback:any) {
        var val = [json.overskrift, json.innhold, json.bilde, json.kat_navn, json.viktighet];
        super.query(
            "INSERT INTO sak(overskrift, innhold, tidOpprettelse, bilde, kat_navn, viktighet) VALUES(?,?,NOW(),?,?,?)",
            val,
            callback
        );
    }

    getArticlesInKat(kat_navn:string, callback:any) {
        super.query(
            "SELECT sak.id, overskrift, innhold, DATE_FORMAT(tidOpprettelse, '%d.%b %Y %k:%i') as tidOpprettelse, bilde from sak JOIN kategori ON sak.kat_navn = kategori.kat_navn WHERE kategori.kat_navn = ?",
            [kat_navn],
            callback
        );
    }

    getKat(callback:any) {
        super.query(
            "SELECT kat_navn FROM kategori",
            [],
            callback
        );
    }

    getNewsfeed(callback:any) {
        super.query(
            "SELECT id, overskrift, DATE_FORMAT(tidOpprettelse, '%d.%b %Y %k:%i') as tidOpprettelse FROM sak ORDER BY tidOpprettelse DESC LIMIT 15",
            [],
            callback
        );
    }

    getArticles(callback:any) {
        super.query(
            "SELECT id, overskrift, innhold, DATE_FORMAT(tidOpprettelse, '%d.%b %Y %k:%i') as tidOpprettelse, bilde, kat_navn, viktighet FROM sak ORDER BY tidOpprettelse DESC",
            [],
            callback
        );
    }
};